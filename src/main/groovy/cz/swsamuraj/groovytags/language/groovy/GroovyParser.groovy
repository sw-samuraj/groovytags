/*
 * Copyright (c) 2015, Vít Kotačka
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package cz.swsamuraj.groovytags.language.groovy

import cz.swsamuraj.groovytags.language.*
import java.util.regex.Matcher

class GroovyParser {

    def filePath
    File file

    def tags = []

    def emptyLineRegex = ~/^\s*/
    def commentOneLineRegex = ~/^\s*\/\//
    def commentMultiLineRegex = ~/^\s*(\/\*|\*|\*\/)/
    def packageRegex = ~/^\s*package\s+([a-zA-Z0-9.-_]+)/
    def classRegex = ~/^\s*(abstract)?\s*(public|private)?\s*class\s+([A-Za-z0-9_]+).*\{/
    def interfaceRegex = ~/^\s*(public|private)?\s*interface\s+([A-Za-z0-9_]+).*\{/
    def enumRegex = ~/^\s*(public|private)?\s*enum\s+([A-Za-z0-9_]+).*\{/
    def constructorRegex = ~/^\s*(public|protected|private)?\s*([A-Z][A-Za-z-0-9_]+)\([A-Za-z0-9 _,]*\).*\{/
    def methodRegex = ~/^\s*(abstract)?\s*(public|protected|private)?\s*(def|[A-Za-z]+)\s+([a-z][A-Za-z-0-9_]+)\([A-Za-z0-9 _,]*\).*\{/
    def fieldRegex = ~/^\s*(public|protected|private)?\s*(def|[A-Za-z]+)\s+([a-zA-Z]+)\s*(=\s*[a-zA-Z0-9_]+)?/

    GroovyParser(file) {
        this.filePath = file
        this.file = new File(file)
    }
    
    def parse() {
        if (file.exists()) {
            println 'File does not exist.'
        } else {
            file.eachLine { line ->
                def tag = parseLine(line)
                if (tag) {
                    tags.add(tag)
                }
            }
        }
    }

    def parseLine(line) {
        def tag

        switch(line) {
            case emptyLineRegex:
                break
            case commentOneLineRegex:
                break
            case commentMultiLineRegex:
                break
            case constructorRegex:
                def matcher = Matcher.lastMatcher
                tag = createTag(matcher, 2,
                                GroovyKind.CONSTRUCTOR.kindShort)
                tag.access = matcher[0][1]
                break
            case fieldRegex:
                def matcher = Matcher.lastMatcher
                tag = createTag(matcher, 3,
                                GroovyKind.METHOD.kindShort)
                tag.access = matcher[0][1]
                break
            case methodRegex:
                def matcher = Matcher.lastMatcher
                tag = createTag(matcher, 4,
                                GroovyKind.METHOD.kindShort)
                tag.access = matcher[0][2]
                break
            case classRegex:
                def matcher = Matcher.lastMatcher
                tag = createTag(matcher, 3,
                                GroovyKind.CLASS.kindShort)
                tag.access = matcher[0][2]
                break
            case interfaceRegex:
                def matcher = Matcher.lastMatcher
                tag = createTag(matcher, 2,
                                GroovyKind.INTERFACE.kindShort)
                tag.access = matcher[0][1]
                break
            case enumRegex:
                def matcher = Matcher.lastMatcher
                tag = createTag(matcher, 2,
                                GroovyKind.ENUM.kindShort)
                tag.access = matcher[0][1]
                break
            case packageRegex:
                tag = createTag(Matcher.lastMatcher, 1,
                                GroovyKind.PACKAGE.kindShort)
                break
        }

        tag
    }

    def createTag(matcher, group, kind) {
        new Tag(tagName: matcher[0][group],
                fileName: filePath,
                exCommand: matcher[0][0],
                kind: kind)
    }

}
