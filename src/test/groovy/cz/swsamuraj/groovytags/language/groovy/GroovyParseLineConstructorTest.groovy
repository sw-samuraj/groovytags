package cz.swsamuraj.groovytags.language.groovy

import org.junit.*

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*

class GroovyParseLineConstructorTest {

    def parser

    def file = 'GroovyTestFile.groovy'

    @Before
    void setUp() {
        parser = new GroovyParser(file)
    }

    @Test
    void parseLinePublicConstructor() {
        def line = 'public GroovyTags() {'
        def tag = assertTag(line)

        assertThat tag.access, equalTo('public')
    }

    @Test
    void parseLinePublicConstructorWhitespace() {
        def line = '    public    GroovyTags() {'
        def tag = assertTag(line)

        assertThat tag.access, equalTo('public')
    }

    @Test
    void parseLineProtectedConstructor() {
        def line = 'protected GroovyTags() {'
        def tag = assertTag(line)

        assertThat tag.access, equalTo('protected')
    }

    @Test
    void parseLinePackageConstructor() {
        def line = 'GroovyTags() {'
        def tag = assertTag(line)

        assertNull tag.access
    }

    @Test
    void parseLineConstructorWithOneParameter() {
        def line = 'GroovyTags(tagName) {'
        assertTag(line)
    }

    @Test
    void parseLineConstructorWithTwoParameters() {
        def line = 'GroovyTags(tagName, tagFile) {'
        assertTag(line)
    }

    @Test
    void parseLineMethodInsteadOfConstructor() {
        def line = 'def groovyTags() {'

        assertThat parser.parseLine(line).kind, not(equalTo(GroovyKind.CONSTRUCTOR.kindShort))
    }

    def assertTag(line) {
        def tag = parser.parseLine(line)

        assertThat tag.tagName, equalTo('GroovyTags')
        assertThat tag.fileName, equalTo(file)
        assertThat tag.exCommand, equalTo(line)
        assertThat tag.kind, equalTo(GroovyKind.CONSTRUCTOR.kindShort)
        assertNull tag.scope

        tag
    }

}
