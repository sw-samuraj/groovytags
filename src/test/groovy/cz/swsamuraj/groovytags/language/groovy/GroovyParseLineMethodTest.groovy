package cz.swsamuraj.groovytags.language.groovy

import org.junit.*

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*

class GroovyParseLineMethodTest {

    def parser

    def file = 'GroovyTestFile.groovy'

    @Before
    void setUp() {
        parser = new GroovyParser(file)
    }

    @Test
    void parseLinePublicMethod() {
        def line = 'public def parseLine() {'
        def tag = assertTag(line)

        assertThat tag.access, equalTo('public')
    }

    @Test
    void parseLinePublicMethodWhitespace() {
        def line = '    public    def    parseLine()    {'
        def tag = assertTag(line)

        assertThat tag.access, equalTo('public')
    }

    @Test
    void parseLineProtectedMethod() {
        def line = 'protected def parseLine() {'
        def tag = assertTag(line)

        assertThat tag.access, equalTo('protected')
    }

    @Test
    void parseLinePackageMethod() {
        def line = 'def parseLine() {'
        def tag = assertTag(line)

        assertNull tag.access
    }

    @Test
    void parseLineAbstractPackageMethod() {
        def line = 'abstract def parseLine() {'
        def tag = assertTag(line)

        assertNull tag.access
    }

    @Test
    void parseLinePackageMethodString() {
        def line = 'String parseLine() {'
        def tag = assertTag(line)

        assertNull tag.access
    }

    @Test
    void parseLinePackageMethodVoid() {
        def line = 'void parseLine() {'
        def tag = assertTag(line)

        assertNull tag.access
    }

    @Test
    void parseLinePackageMethodBoolean() {
        def line = 'boolean parseLine() {'
        def tag = assertTag(line)

        assertNull tag.access
    }

    @Test
    void parseLineMethodWithOneParameter() {
        def line = 'def parseLine(line) {'
        assertTag(line)
    }

    @Test
    void parseLineMethodWithTwoParameters() {
        def line = 'def parseLine(line, lineSeparator) {'
        assertTag(line)
    }

    @Test
    void parseLineConstructorInsteadOfMethod() {
        def line = 'ParseLine() {'

        assertThat parser.parseLine(line).kind, not(equalTo(GroovyKind.METHOD.kindShort))
    }

    def assertTag(line) {
        def tag = parser.parseLine(line)

        assertThat tag.tagName, equalTo('parseLine')
        assertThat tag.fileName, equalTo(file)
        assertThat tag.exCommand, equalTo(line)
        assertThat tag.kind, equalTo(GroovyKind.METHOD.kindShort)
        assertNull tag.scope

        tag
    }

}
