package cz.swsamuraj.groovytags.language.groovy

import org.junit.*

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*

class GroovyParseLineClassTest {

    def parser

    def file = 'GroovyTestFile.groovy'

    @Before
    void setUp() {
        parser = new GroovyParser(file)
    }

    @Test
    void parseLineClass() {
        def line = 'class GroovyTags {'
        def tag = assertTag(line)

        assertNull tag.access
    }

    @Test
    void parseLineClassImplementingInterface() {
        def line = 'class GroovyTags implements Serializable {'
        def tag = assertTag(line)

        assertNull tag.access
    }

    @Test
    void parseLineClassExtendingClass() {
        def line = 'class GroovyTags extends AbstractTag {'
        def tag = assertTag(line)

        assertNull tag.access
    }

    @Test
    void parseLineClassImplementingInterfaceAndExtendingClass() {
        def line = 'class GroovyTags implements Serializable extends AbstractTag {'
        def tag = assertTag(line)

        assertNull tag.access
    }

    @Test
    void parseLinePublicClass() {
        def line = 'public class GroovyTags {'
        def tag = assertTag(line)

        assertThat tag.access, equalTo('public')
    }

    @Test
    void parseLinePrivateClass() {
        def line = 'private class GroovyTags {'
        def tag = assertTag(line)

        assertThat tag.access, equalTo('private')
    }

    @Test
    void parseLineAbstractClass() {
        def line = 'abstract class GroovyTags {'
        def tag = assertTag(line)

        assertNull tag.access
    }

    @Test
    void parseLineAbstractPublicClass() {
        def line = 'abstract public class GroovyTags {'
        def tag = assertTag(line)

        assertThat tag.access, equalTo('public')
    }

    @Test
    void parseLineClassWithWhitespace() {
        def line = '   abstract   public   class   GroovyTags   {'
        def tag = assertTag(line)

        assertThat tag.access, equalTo('public')
    }

    @Test
    void parseLineNoClass() {
        def line = 'public GroovyTags {'

        assertNull parser.parseLine(line)
    }

    @Test
    void parseLineClassWithoutName() {
        def line = 'class {'

        assertNull parser.parseLine(line)
    }

    def assertTag(line) {
        def tag = parser.parseLine(line)

        assertThat tag.tagName, equalTo('GroovyTags')
        assertThat tag.fileName, equalTo(file)
        assertThat tag.exCommand, equalTo(line)
        assertThat tag.kind, equalTo(GroovyKind.CLASS.kindShort)
        assertNull tag.scope

        tag
    }

}
