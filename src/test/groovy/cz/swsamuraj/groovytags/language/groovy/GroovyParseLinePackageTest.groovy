package cz.swsamuraj.groovytags.language.groovy

import org.junit.*

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*

class GroovyParseLinePackageTest {

    def parser

    def file = 'GroovyTestFile.groovy'

    @Before
    void setUp() {
        parser = new GroovyParser(file)
    }

    @Test
    void parseLinePackage() {
        def line = 'package cz.swsamuraj.groovytags'

        assertTag(line)
    }

    @Test
    void parseLinePackageLeadingWhitespace() {
        def line = '    package cz.swsamuraj.groovytags'

        assertTag(line)
    }

    @Test
    void parseLinePackageMiddleWhitespace() {
        def line = '    package    cz.swsamuraj.groovytags'

        assertTag(line)
    }

    @Test
    void parseLineNoPackage() {
        def line = 'cz.swsamuraj.groovytags'

        assertNull parser.parseLine(line)
    }

    @Test
    void parseLineReverseOrderPackage() {
        def line = 'cz.swsamuraj.groovytags package'

        assertNull parser.parseLine(line)
    }

    def assertTag(line) {
        def tag = parser.parseLine(line)

        assertThat tag.tagName, equalTo('cz.swsamuraj.groovytags')
        assertThat tag.fileName, equalTo(file)
        assertThat tag.exCommand, equalTo(line)
        assertThat tag.kind, equalTo(GroovyKind.PACKAGE.kindShort)
        assertNull tag.scope
        assertNull tag.access
    }

}
