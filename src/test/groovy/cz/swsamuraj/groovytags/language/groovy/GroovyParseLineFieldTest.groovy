package cz.swsamuraj.groovytags.language.groovy

import org.junit.*

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*

class GroovyParseLineFieldTest {

    def parser

    def file = 'GroovyTestFile.groovy'

    @Before
    void setUp() {
        parser = new GroovyParser(file)
    }

    @Test
    void parseLinePublicField() {
        def line = 'public def ultimateQuestion'
        def tag = assertTag(line)

        assertThat tag.access, equalTo('public')
    }

    @Test
    void parseLinePublicFieldAssigned() {
        def line = 'public def ultimateQuestion = 42'
        def tag = assertTag(line)

        assertThat tag.access, equalTo('public')
    }

    @Test
    void parseLinePublicFieldWhitespace() {
        def line = '    public    def    ultimateQuestion'
        def tag = assertTag(line)

        assertThat tag.access, equalTo('public')
    }

    @Test
    void parseLinePublicFieldWhitespaceAssigned() {
        def line = '    public    def    ultimateQuestion    =    42'
        def tag = assertTag(line)

        assertThat tag.access, equalTo('public')
    }

    @Test
    void parseLinePublicFieldNoWhitespaceAroundEqualSign() {
        def line = 'public def ultimateQuestion=42'
        def tag = assertTag(line)

        assertThat tag.access, equalTo('public')
    }

    @Test
    void parseLineProtectedField() {
        def line = 'protected def ultimateQuestion'
        def tag = assertTag(line)

        assertThat tag.access, equalTo('protected')
    }

    @Test
    void parseLineProtectedFieldAssigned() {
        def line = 'protected def ultimateQuestion = 42'
        def tag = assertTag(line)

        assertThat tag.access, equalTo('protected')
    }

    @Test
    void parseLinePackageField() {
        def line = 'def ultimateQuestion'
        def tag = assertTag(line)

        assertNull tag.access
    }

    @Test
    void parseLinePackageFieldAssigned() {
        def line = 'def ultimateQuestion = 42'
        def tag = assertTag(line)

        assertNull tag.access
    }

    @Test
    void parseLinePackageFieldString() {
        def line = 'String ultimateQuestion'
        def tag = assertTag(line)

        assertNull tag.access
    }

    @Test
    void parseLinePackageFieldBoolean() {
        def line = 'boolean ultimateQuestion'
        def tag = assertTag(line)

        assertNull tag.access
    }

    @Test
    void parseLinePackageFieldWithoutType() {
        def line = 'parseLine'

        assertNull parser.parseLine(line)
    }

    @Test
    void parseLinePackageFieldWithoutTypeAssigned() {
        def line = 'parseLine = 42'

        assertNull parser.parseLine(line)
    }

    def assertTag(line) {
        def tag = parser.parseLine(line)

        assertThat tag.tagName, equalTo('ultimateQuestion')
        assertThat tag.fileName, equalTo(file)
        assertThat tag.exCommand, equalTo(line)
        assertThat tag.kind, equalTo(GroovyKind.METHOD.kindShort)
        assertNull tag.scope

        tag
    }

}
