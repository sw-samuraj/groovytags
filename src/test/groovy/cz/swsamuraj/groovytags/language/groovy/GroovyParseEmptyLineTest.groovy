package cz.swsamuraj.groovytags.language.groovy

import org.junit.*

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*

class GroovyParseEmptyLineTest {

    def parser

    def file = 'GroovyTestFile.groovy'

    @Before
    void setUp() {
        parser = new GroovyParser(file)
    }

    @Test
    void parseLineFourSpaces() {
        def line = '    '

        assertNull parser.parseLine(line)
    }

    @Test
    void parseNewLine() {
        def line = '\n'

        assertNull parser.parseLine(line)
    }

}
