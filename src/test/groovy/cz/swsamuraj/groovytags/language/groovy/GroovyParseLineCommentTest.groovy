package cz.swsamuraj.groovytags.language.groovy

import org.junit.*

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*

class GroovyParseLineCommentTest {

    def parser

    def file = 'GroovyTestFile.groovy'

    @Before
    void setUp() {
        parser = new GroovyParser(file)
    }

    @Test
    void parseLineOneLineComment() {
        def line = '//'

        assertNull parser.parseLine(line)
    }

    @Test
    void parseLineOneLineCommentLeadingWhitespace() {
        def line = '    //'

        assertNull parser.parseLine(line)
    }

    @Test
    void parseLineOneLineCommentAndText() {
        def line = '// Some comment'

        assertNull parser.parseLine(line)
    }

    @Test
    void parseLineMultiLineCommentAndTextBeginning() {
        def line = '/* Some comment'

        assertNull parser.parseLine(line)
    }

    @Test
    void parseLineMultiLineCommentAndTextMiddle() {
        def line = ' * Some comment'

        assertNull parser.parseLine(line)
    }

    @Test
    void parseLineMultiLineCommentAndTextEnd() {
        def line = ' */'

        assertNull parser.parseLine(line)
    }

}
