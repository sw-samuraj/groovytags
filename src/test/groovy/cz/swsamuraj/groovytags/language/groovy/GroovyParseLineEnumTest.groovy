package cz.swsamuraj.groovytags.language.groovy

import org.junit.*

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*

class GroovyParseLineEnumTest {

    def parser

    def file = 'GroovyTestFile.groovy'

    @Before
    void setUp() {
        parser = new GroovyParser(file)
    }

    @Test
    void parseLineEnum() {
        def line = 'enum GroovyKind {'
        def tag = assertTag(line)

        assertNull tag.access
    }

    @Test
    void parseLinePublicEnum() {
        def line = 'public enum GroovyKind {'
        def tag = assertTag(line)

        assertThat tag.access, equalTo('public')
    }

    @Test
    void parseLinePrivateEnum() {
        def line = 'private enum GroovyKind {'
        def tag = assertTag(line)

        assertThat tag.access, equalTo('private')
    }

    @Test
    void parseLineEnumWithWhitespace() {
        def line = '   public   enum   GroovyKind   {'
        def tag = assertTag(line)

        assertThat tag.access, equalTo('public')
    }

    @Test
    void parseLineNoEnum() {
        def line = 'public GroovyKind {'

        assertNull parser.parseLine(line)
    }

    @Test
    void parseLineEnumWithoutName() {
        def line = 'enum {'

        assertNull parser.parseLine(line)
    }

    def assertTag(line) {
        def tag = parser.parseLine(line)

        assertThat tag.tagName, equalTo('GroovyKind')
        assertThat tag.fileName, equalTo(file)
        assertThat tag.exCommand, equalTo(line)
        assertThat tag.kind, equalTo(GroovyKind.ENUM.kindShort)
        assertNull tag.scope

        tag
    }

}
