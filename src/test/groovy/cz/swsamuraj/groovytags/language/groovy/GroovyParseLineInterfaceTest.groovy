package cz.swsamuraj.groovytags.language.groovy

import org.junit.*

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*

class GroovyParseLineInterfaceTest {

    def parser

    def file = 'GroovyTestFile.groovy'

    @Before
    void setUp() {
        parser = new GroovyParser(file)
    }

    @Test
    void parseLineInterface() {
        def line = 'interface Parser {'
        def tag = assertTag(line)

        assertNull tag.access
    }

    @Test
    void parseLineInterfaceExtendingInterface() {
        def line = 'interface Parser extends BasicParser {'
        def tag = assertTag(line)

        assertNull tag.access
    }

    @Test
    void parseLinePublicInterface() {
        def line = 'public interface Parser {'
        def tag = assertTag(line)

        assertThat tag.access, equalTo('public')
    }

    @Test
    void parseLinePrivateInterface() {
        def line = 'private interface Parser {'
        def tag = assertTag(line)

        assertThat tag.access, equalTo('private')
    }

    @Test
    void parseLineInterfaceWithWhitespace() {
        def line = '   public   interface   Parser   {'
        def tag = assertTag(line)

        assertThat tag.access, equalTo('public')
    }

    @Test
    void parseLineNoInterface() {
        def line = 'public Parser {'

        assertNull parser.parseLine(line)
    }

    @Test
    void parseLineInterfaceWithoutName() {
        def line = 'interface {'

        assertNull parser.parseLine(line)
    }

    def assertTag(line) {
        def tag = parser.parseLine(line)

        assertThat tag.tagName, equalTo('Parser')
        assertThat tag.fileName, equalTo(file)
        assertThat tag.exCommand, equalTo(line)
        assertThat tag.kind, equalTo(GroovyKind.INTERFACE.kindShort)
        assertNull tag.scope

        tag
    }

}
