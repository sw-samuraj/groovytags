package cz.swsamuraj.groovytags.output

import org.junit.*

import static org.junit.Assert.*
import static org.hamcrest.CoreMatchers.*

class OutputTest {

    @Test
    void testHelpOutput() {
        def help = new HelpOutput()

        assertThat help.getOutputAsString().trim(), equalTo('GroovyTags help')
    }

    @Test
    void testLicenseOutput() {
        def license = new LicenseOutput()

        assertThat license.getOutputAsString().trim(), equalTo('BSD License')
    }

    @Test
    void testVersionOutput() {
        def version = new VersionOutput()

        assertThat version.getOutputAsString().trim(), equalTo('GroovyTags 42')
    }

    @Test
    void testLanguageOutput() {
        def language = new LanguageOutput()

        assertThat language.getOutputAsString(),
                   equalTo('Gradle\nGroovy')
    }

}
